// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package acronym should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package acronym

import "strings"
import "unicode"

// Abbreviate should have a comment documenting it.
func Abbreviate(s string) string {
	s = strings.ReplaceAll(s, "-", " ")

	p := strings.Split(s, " ")
	r := make([]rune, 0, len(p))

	for _, v := range p {
		for _, c := range v {
			if unicode.IsLetter(c) {
				r = append(r, unicode.ToUpper(c))
				break
			}
		}
	}

	return string(r)
}
