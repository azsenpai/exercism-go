package account

import "sync"

type Account struct {
	sync.Mutex
	balance  int64
	isClosed bool
}

func Open(initialDeposit int64) *Account {
	if initialDeposit < 0 {
		return nil
	}

	return &Account{balance: initialDeposit, isClosed: false}
}

func (a *Account) Close() (payout int64, ok bool) {
	a.Lock()
	defer a.Unlock()

	if a.isClosed {
		return 0, false
	} else {
		a.isClosed = true
	}

	payout = a.balance
	ok = true

	return
}

func (a *Account) Balance() (balance int64, ok bool) {
	a.Lock()
	defer a.Unlock()

	if a.isClosed {
		return 0, false
	}

	balance = a.balance
	ok = true

	return
}

func (a *Account) Deposit(amount int64) (newBalance int64, ok bool) {
	a.Lock()
	defer a.Unlock()

	if a.isClosed {
		return 0, false
	}

	newBalance = a.balance + amount
	ok = newBalance >= 0

	if ok {
		a.balance = newBalance
	} else {
		newBalance = 0
	}

	return
}
