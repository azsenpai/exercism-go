package space

import "strings"

type Planet string

const EarthOrbitalPeriodInSeconds = 31557600.0

var orbitalPeriodInSeconds = map[Planet]float64{
	"earth":   EarthOrbitalPeriodInSeconds,
	"mercury": EarthOrbitalPeriodInSeconds * 0.2408467,
	"venus":   EarthOrbitalPeriodInSeconds * 0.61519726,
	"mars":    EarthOrbitalPeriodInSeconds * 1.8808158,
	"jupiter": EarthOrbitalPeriodInSeconds * 11.862615,
	"saturn":  EarthOrbitalPeriodInSeconds * 29.447498,
	"uranus":  EarthOrbitalPeriodInSeconds * 84.016846,
	"neptune": EarthOrbitalPeriodInSeconds * 164.79132,
}

func Age(seconds float64, planet Planet) float64 {
	planet = Planet(strings.ToLower(string(planet)))

	if p, ok := orbitalPeriodInSeconds[planet]; ok {
		return seconds / p
	}

	return 0
}
