package cryptosquare

import (
	"math"
	"unicode"
)

// Encode return string
func Encode(input string) string {
	s := make([]rune, 0, len(input))

	for _, v := range input {
		if unicode.IsLetter(v) || unicode.IsDigit(v) {
			s = append(s, unicode.ToLower(v))
		}
	}

	r := int(math.Sqrt(float64(len(s))))
	c := r

	if r*c < len(s) {
		c++
	}

	if r*c < len(s) {
		r++
	}

	output := make([]rune, 0, r*c+c)

	for j := 0; j < c; j++ {
		for i := 0; i < r; i++ {
			k := i*c + j
			v := ' '
			if k < len(s) {
				v = s[k]
			}
			output = append(output, v)
		}
		if j+1 < c {
			output = append(output, ' ')
		}
	}

	return string(output)
}
