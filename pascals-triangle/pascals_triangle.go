package pascal

func Triangle(n int) (r [][]int) {
	if n <= 0 {
		return
	}

	r = make([][]int, n)

	r[0] = make([]int, 1)
	r[0][0] = 1

	for i := 1; i < n; i++ {
		r[i] = make([]int, i+1)
		for j := 0; j <= i; j++ {
			a, b := 0, 0

			if j-1 >= 0 {
				a = r[i-1][j-1]
			}

			if j <= i-1 {
				b = r[i-1][j]
			}

			r[i][j] = a + b
		}
	}

	return
}
