package paasio

import (
	"io"
	"sync"
)

type Counter struct {
	sync.Mutex
	n    int64
	nops int
}

type RCounter struct {
	Counter
	io.Reader
}

type WCounter struct {
	Counter
	io.Writer
}

type RWCounter struct {
	ReadCounter
	WriteCounter
}

func NewReadCounter(r io.Reader) ReadCounter {
	return &RCounter{Reader: r}
}

func NewWriteCounter(w io.Writer) WriteCounter {
	return &WCounter{Writer: w}
}

func NewReadWriteCounter(rw io.ReadWriter) ReadWriteCounter {
	return &RWCounter{
		NewReadCounter(rw),
		NewWriteCounter(rw),
	}
}

func (c *RCounter) Read(p []byte) (n int, err error) {
	c.Lock()
	defer c.Unlock()

	n, err = c.Reader.Read(p)

	c.n += int64(n)
	c.nops += 1

	return
}

func (c *RCounter) ReadCount() (n int64, nops int) {
	c.Lock()
	defer c.Unlock()

	return c.n, c.nops
}

func (c *WCounter) Write(p []byte) (n int, err error) {
	c.Lock()
	defer c.Unlock()

	n, err = c.Writer.Write(p)

	c.n += int64(n)
	c.nops += 1

	return
}

func (c *WCounter) WriteCount() (n int64, nops int) {
	c.Lock()
	defer c.Unlock()

	return c.n, c.nops
}
