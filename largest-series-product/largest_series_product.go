package lsproduct

import (
	"errors"
	"strconv"
)

// LargestSeriesProduct return (int, error)
func LargestSeriesProduct(digits string, span int) (int, error) {
	if span < 0 {
		return 0, errors.New("span must be greater than zero")
	}

	if len(digits) < span {
		return 0, errors.New("span must be smaller than string length")
	}

	r := 0

	for i := 0; i <= len(digits)-span; i++ {
		p := 1

		for j := 0; j < span; j++ {
			d, err := strconv.Atoi(digits[i+j : i+j+1])

			if err != nil {
				return 0, errors.New("digits input must only contain digits")
			}

			p *= d
		}

		if r < p {
			r = p
		}
	}

	return r, nil
}
