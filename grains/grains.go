package grains

import "fmt"

const Size = 64

func Square(n int) (uint64, error) {
	switch {
	case n < 1:
		return 0, fmt.Errorf("n must be greater than 0")
	case n > Size:
		return 0, fmt.Errorf("n must be less than %d", Size)
	default:
		return 1 << (n - 1), nil
	}
}

func Total() uint64 {
	return (1 << Size) - 1
}
