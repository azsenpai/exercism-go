package raindrops

import (
	"fmt"
	"strings"
)

func Convert(n int) string {
	var b strings.Builder

	if n%3 == 0 {
		fmt.Fprint(&b, "Pling")
	}

	if n%5 == 0 {
		fmt.Fprint(&b, "Plang")
	}

	if n%7 == 0 {
		fmt.Fprint(&b, "Plong")
	}

	if r := b.String(); len(r) > 0 {
		return r
	}

	return fmt.Sprintf("%d", n)
}
