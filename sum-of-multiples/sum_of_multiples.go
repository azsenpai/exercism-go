package summultiples

// SumMultiples return int
func SumMultiples(limit int, divisors ...int) int {
	s := 0

	for i := 1; i < limit; i++ {
		for _, v := range divisors {
			if v == 0 || v > i {
				continue
			}
			if i%v == 0 {
				s += i
				break
			}
		}
	}

	return s
}
