package isogram

import "unicode"

func IsIsogram(s string) bool {
	used := make(map[rune]bool, len(s))

	for _, c := range s {
		if !unicode.IsLetter(c) {
			continue
		}

		t := unicode.ToLower(c)

		if used[t] {
			return false
		}

		used[t] = true
	}

	return true
}
