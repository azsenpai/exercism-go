package sieve

// Sieve return []int
func Sieve(limit int) []int {
	isPrime := make([]bool, limit+1)

	for i := 2; i <= limit; i++ {
		isPrime[i] = true
	}

	for i := 2; i*i <= limit; i++ {
		if !isPrime[i] {
			continue
		}

		for j := i * i; j <= limit; j += i {
			isPrime[j] = false
		}
	}

	p := make([]int, 0)

	for i := 2; i <= limit; i++ {
		if isPrime[i] {
			p = append(p, i)
		}
	}

	return p
}
