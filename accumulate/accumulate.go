package accumulate

func Accumulate(input []string, fn func (string) string) []string {
	r := make([]string, len(input))

	for i, v := range input {
		r[i] = fn(v)
	}

	return r
}
