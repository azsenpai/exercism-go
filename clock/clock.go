package clock

import "fmt"

type Clock struct {
	h, m int
}

func New(hour, minute int) Clock {
	if minute < 0 {
		hour += (minute - 59) / 60
	} else {
		hour += minute / 60
	}

	minute = (60 + minute % 60) % 60
	hour = (24 + hour % 24) % 24

	return Clock{hour, minute}
}

func (c Clock) String() string {
	return fmt.Sprintf("%02d:%02d", c.h, c.m)
}

func (c Clock) Add(minutes int) Clock {
	return New(c.h, c.m + minutes)
}

func (c Clock) Subtract(minutes int) Clock {
	return New(c.h, c.m - minutes)
}
