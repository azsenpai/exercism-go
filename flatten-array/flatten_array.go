package flatten

func Flatten(input interface{}) []interface{} {
	r := []interface{}{}

	switch v := input.(type) {
	case int:
		r = append(r, v)
	case []interface{}:
		for _, item := range v {
			r = append(r, Flatten(item)...)
		}
	}

	return r
}
