package secret

var events = [...]string{"wink", "double blink", "close your eyes", "jump"}

func Handshake(code uint) []string {
	r := make([]string, 0, len(events))

	for i := 0; i < len(events); i++ {
		if code & (uint(1) << i) > 0 {
			r = append(r, events[i])
		}
	}

	if code & (uint(1) << len(events)) > 0 {
		for i, j := 0, len(r)-1; i < j; i, j = i+1, j-1 {
			r[i], r[j] = r[j], r[i]
		}
	}

	return r
}
