package diffsquares

func Sum(n int, f func(int) int) int {
	r := 0

	for i := 1; i <= n; i ++ {
		r += f(i)
	}

	return r
}

func SquareOfSum(n int) int {
	s := Sum(n, func(x int) int {
		return x
	})

	return s*s
}

func SumOfSquares(n int) int {
	return Sum(n, func(x int) int {
		return x*x
	})
}

func Difference(n int) int {
	return SquareOfSum(n) - SumOfSquares(n)
}
