package etl

import "strings"

// Transform return map[string]int
func Transform(input map[int][]string) map[string]int {
	r := make(map[string]int)

	for k, v := range input {
		for _, s := range v {
			r[strings.ToLower(s)] = k
		}
	}

	return r
}
