package sublist

type Relation string

const (
	RelationEqual     Relation = "equal"
	RelationSuperlist Relation = "superlist"
	RelationSublist   Relation = "sublist"
	RelationUnequal   Relation = "unequal"
)

func IsEqual(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}

	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}

func IsSuperlist(a, b []int) bool {
	if len(a) <= len(b) {
		return false
	}

	for i := 0; i <= len(a)-len(b); i++ {
		if IsEqual(a[i:i+len(b)], b) {
			return true
		}
	}

	return false
}

func IsSublist(a, b []int) bool {
	return IsSuperlist(b, a)
}

func Sublist(a, b []int) Relation {
	switch {
	case IsEqual(a, b):
		return RelationEqual
	case IsSuperlist(a, b):
		return RelationSuperlist
	case IsSublist(a, b):
		return RelationSublist
	default:
		return RelationUnequal
	}
}
