// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package bob should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package bob

import (
	"strings"
	"unicode"
)

const (
	answerSure     = "Sure."
	answerChillOut = "Whoa, chill out!"
	answerCalmDown = "Calm down, I know what I'm doing!"
	answerFine     = "Fine. Be that way!"
	answerWhatever = "Whatever."
)

func IsUpper(s string) (ok bool, hasLetter bool) {
	r := []rune(s)

	ok = true
	hasLetter = false

	for _, v := range r {
		if !unicode.IsLetter(v) {
			continue
		}

		hasLetter = true
		ok = ok && unicode.IsUpper(v)

		if !ok {
			break
		}
	}

	return
}

// Hey should have a comment documenting it.
func Hey(remark string) string {
	s := strings.TrimSpace(remark)

	switch {
	case len(s) == 0:
		return answerFine

	case s[len(s)-1] == '?':
		if ok, hasLetter := IsUpper(s); ok && hasLetter {
			return answerCalmDown
		} else {
			return answerSure
		}

	default:
		if ok, hasLetter := IsUpper(s); ok && hasLetter {
			return answerChillOut
		} else {
			return answerWhatever
		}
	}
}
