package pythagorean

import "math"

// Triplet [3]int
type Triplet [3]int

// Range return []Triplet
func Range(min, max int) []Triplet {
	r := make([]Triplet, 0)

	for a := min; a < max; a++ {
		for b := a + 1; true; b++ {
			cc := a*a + b*b
			c := int(math.Sqrt(float64(cc)))

			if c > max {
				break
			}

			if c*c == cc {
				r = append(r, Triplet{a, b, c})
			}
		}
	}

	return r
}

// Sum return []Triplet
func Sum(p int) []Triplet {
	r := make([]Triplet, 0)

	for _, t := range Range(1, p) {
		if t[0]+t[1]+t[2] == p {
			r = append(r, t)
		}
	}

	return r
}
