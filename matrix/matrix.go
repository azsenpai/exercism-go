package matrix

import (
	"strings"
	"strconv"
	"errors"
)

type Matrix [][]int

func New(s string) (Matrix, error) {
	rows := strings.Split(s, "\n")

	rowSize := len(rows)
	colSize := -1

	m := make(Matrix, rowSize)

	for i := 0; i < rowSize; i++ {
		cols := strings.Fields(rows[i])

		if colSize == -1 {
			colSize = len(cols)
		} else if colSize != len(cols) {
			return nil, errors.New("uneven rows")
		}

		m[i] = make([]int, colSize)

		for j, v := range cols {
			var err error
			m[i][j], err = strconv.Atoi(v)

			if err != nil {
				return nil, err
			}
		}
	}

	return m, nil
}

func (m Matrix) Rows() [][]int {
	rowSize := len(m)
	colSize := 0

	if rowSize > 0 {
		colSize = len(m[0])
	}

	n := make([][]int, rowSize)

	for i := 0; i < rowSize; i++ {
		n[i] = make([]int, colSize)
		for j := 0; j < colSize; j++ {
			n[i][j] = m[i][j]
		}
	}

	return n
}

func (m Matrix) Cols() [][]int {
	rowSize := len(m)
	colSize := 0

	if rowSize > 0 {
		colSize = len(m[0])
	}

	n := make([][]int, colSize)

	for j := 0; j < colSize; j++ {
		n[j] = make([]int, rowSize)
		for i := 0; i < rowSize; i++ {
			n[j][i] = m[i][j]
		}
	}

	return n
}

func (m Matrix) Set(r, c, v int) bool {
	rowSize := len(m)
	colSize := 0

	if rowSize > 0 {
		colSize = len(m[0])
	}

	if 0 <= r && r < rowSize && 0 <= c && c < colSize {
		m[r][c] = v
		return true
	}

	return false
}
