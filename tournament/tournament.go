package tournament

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"
)

func Tally(rd io.Reader, writer io.Writer) error {
	reader := bufio.NewReader(rd)

	mp := map[string]int{}
	mw := map[string]int{}
	ml := map[string]int{}
	md := map[string]int{}

	for {
		s, err := reader.ReadString('\n')
		s = strings.TrimSpace(s)

		if len(s) == 0 || s[0] == '#' {
			if err != nil {
				break
			}
			continue
		}

		parts := strings.Split(s, ";")

		if len(parts) != 3 {
			return errors.New("Invalid data")
		}

		teamA, teamB, status := parts[0], parts[1], parts[2]

		mp[teamA] += 1
		mp[teamB] += 1

		switch status {
		case "win":
			mw[teamA] += 1
			ml[teamB] += 1
		case "loss":
			mw[teamB] += 1
			ml[teamA] += 1
		case "draw":
			md[teamA] += 1
			md[teamB] += 1
		default:
			return errors.New("Undefined match status")
		}

		if err != nil {
			break
		}
	}

	teams := make([]string, 0, len(mp))

	for k := range mp {
		teams = append(teams, k)
	}

	sort.Slice(teams, func(i, j int) bool {
		scoreA := mw[teams[i]]*3 + md[teams[i]]
		scoreB := mw[teams[j]]*3 + md[teams[j]]

		return scoreA > scoreB || (scoreA == scoreB && teams[i] < teams[j])
	})

	headFormat := "%-30s | %2s | %2s | %2s | %2s | %2s\n"
	bodyFormat := "%-30s | %2d | %2d | %2d | %2d | %2d\n"

	fmt.Fprintf(writer, headFormat, "Team", "MP", "W", "D", "L", "P")

	for _, v := range teams {
		fmt.Fprintf(writer, bodyFormat, v, mp[v], mw[v], md[v], ml[v], mw[v]*3+md[v])
	}

	return nil
}
