package robotname

import (
	"errors"
	"math/rand"
)

type Robot struct {
	name string
}

var used = map[string]bool{}
const maxLimits = 26*26*10*10*10

func RandName() string {
	return string([]rune{
		'A' + rune(rand.Intn(26)),
		'A' + rune(rand.Intn(26)),
		'0' + rune(rand.Intn(10)),
		'0' + rune(rand.Intn(10)),
		'0' + rune(rand.Intn(10)),
	})
}

func NewName() (string, error) {
	if len(used) >= maxLimits {
		return "", errors.New("No available name")
	}

	var name string

	for name = RandName(); used[name]; name = RandName() {
	}
	used[name] = true

	return name, nil
}

func (r *Robot) Name() (string, error) {
	if len(r.name) == 0 {
		name, err := NewName()
		r.name = name

		if err != nil {
			return "", err
		}
	}

	return r.name, nil
}

func (r *Robot) Reset() {
	r.name = ""
}
