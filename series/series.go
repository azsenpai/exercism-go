package series

func All(n int, s string) []string {
	var r []string
	var m int

	if m = len(s) - n; m >= 0 {
		r = make([]string, m+1)
	} else {
		return []string{}
	}

	for i := 0; i <= m; i++ {
		r[i] = s[i:i+n]
	}

	return r
}

func UnsafeFirst(n int, s string) string {
	first, ok := First(n, s)

	if !ok {
		return s
	}

	return first
}

func First(n int, s string) (first string, ok bool) {
	if len(s) - n >= 0 {
		first, ok = s[:n], true
	}

	return
}
