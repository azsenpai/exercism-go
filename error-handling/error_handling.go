package erratum

func Use(opener ResourceOpener, input string) (err error) {
	r, err := opener()

	if err != nil {
		if _, ok := err.(TransientError); ok {
			return Use(opener, input)
		}
		return
	}

	defer r.Close()

	defer func() {
		v := recover()

		if v == nil {
			return
		}

		switch verr := v.(type) {
		case FrobError:
			r.Defrob(verr.defrobTag)
			err = verr
		case error:
			err = verr
		}
	}()

	r.Frob(input)

	return
}
