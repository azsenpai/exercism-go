package twelve

import (
	"strings"
	"fmt"
)

const songFormat = "On the %s day of Christmas my true love gave to me: %s."

var day = map[int]string{
	1:  "first",
	2:  "second",
	3:  "third",
	4:  "fourth",
	5:  "fifth",
	6:  "sixth",
	7:  "seventh",
	8:  "eighth",
	9:  "ninth",
	10: "tenth",
	11: "eleventh",
	12: "twelfth",
}

var present = map[int]string{
	1:  "a Partridge in a Pear Tree",
	2:  "two Turtle Doves",
	3:  "three French Hens",
	4:  "four Calling Birds",
	5:  "five Gold Rings",
	6:  "six Geese-a-Laying",
	7:  "seven Swans-a-Swimming",
	8:  "eight Maids-a-Milking",
	9:  "nine Ladies Dancing",
	10: "ten Lords-a-Leaping",
	11: "eleven Pipers Piping",
	12: "twelve Drummers Drumming",
}

func Verse(n int) string {
	presents := ""
	elems := make([]string, 0, n-1)

	for i := n; i > 1; i-- {
		elems = append(elems, present[i])
	}

	if len(elems) > 0 {
		presents = strings.Join(elems, ", ") + ", and " + present[1]
	} else {
		presents = present[1]
	}

	return fmt.Sprintf(songFormat, day[n], presents)
}

func Song() string {
	s := make([]string, 0, 12)

	for i := 1; i <= 12; i++ {
		s = append(s, Verse(i))
	}

	return strings.Join(s, "\n")
}
