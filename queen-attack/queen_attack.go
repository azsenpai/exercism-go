package queenattack

import "errors"

// Point struct
type Point struct {
	X, Y int
}

// NewPoint return (Point, error)
func NewPoint(s string) (Point, error) {
	if len(s) != 2 || s[0] < 'a' || s[0] > 'h' || s[1] < '1' || s[1] > '8' {
		return Point{}, errors.New("Invalid argument")
	}

	return Point{int(s[0] - 'a'), int(s[1] - '1')}, nil
}

// CanQueenAttack indicate whether or not they are positioned
// so that they can attack each other
func CanQueenAttack(a, b string) (bool, error) {
	p, err := NewPoint(a)
	if err != nil {
		return false, err
	}

	q, err := NewPoint(b)
	if err != nil {
		return false, err
	}

	if p == q {
		return false, errors.New("Same positions")
	}

	return p.CanQueenAttack(q), nil
}

// CanQueenAttack return bool
func (p Point) CanQueenAttack(q Point) bool {
	dx := p.X - q.X
	dy := p.Y - q.Y

	if dx < 0 {
		dx = -dx
	}

	if dy < 0 {
		dy = -dy
	}

	return p.X == q.X || p.Y == q.Y || dx == dy
}
