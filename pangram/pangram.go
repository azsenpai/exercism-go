package pangram

import (
	"strings"
)

// IsPangram return bool
func IsPangram(input string) bool {
	used := make(map[rune]bool)

	for _, r := range strings.ToLower(input) {
		used[r] = true
	}

	for r := 'a'; r <= 'z'; r++ {
		if !used[r] {
			return false
		}
	}

	return true
}
