package luhn

import (
	"strings"
	"unicode"
)

func Valid(s string) bool {
	s = strings.ReplaceAll(s, " ", "")

	if len(s) <= 1 {
		return false
	}

	for _, c := range s {
		if !unicode.IsDigit(c) {
			return false
		}
	}

	numbers := make([]int, 0, len(s))
	sum := 0

	for _, c := range s {
		numbers = append(numbers, int(c - '0'))
	}

	for i := len(numbers)-2; i >= 0; i -= 2 {
		numbers[i] *= 2
		if numbers[i] > 9 {
			numbers[i] -= 9
		}
	}

	for _, a := range numbers {
		sum += a
	}

	return sum % 10 == 0
}
