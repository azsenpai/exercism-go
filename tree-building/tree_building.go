package tree

import (
	"errors"
	"sort"
)

type Record struct {
	ID, Parent int
}

type Node struct {
	ID       int
	Children []*Node
}

func Build(records []Record) (*Node, error) {
	m := make(map[int]*Node, len(records))

	sort.Slice(records, func(i, j int) bool {
		return records[i].ID < records[j].ID
	})

	for _, r := range records {
		switch {
		case r.ID < 0 || r.ID >= len(records):
			return nil, errors.New("Invalid ID")

		case r.ID != 0 && r.ID <= r.Parent:
			return nil, errors.New("Invalid Parent")

		case r.ID == 0 && r.ID != r.Parent:
			return nil, errors.New("root node: Invalid Parent")

		default:
			n, ok := m[r.ID]
			if ok {
				return nil, errors.New("Duplicate node")
			} else {
				n = &Node{ID: r.ID}
				m[r.ID] = n
			}

			if r.ID == 0 {
				continue
			}

			p, ok := m[r.Parent]
			if !ok {
				return nil, errors.New("Parent not found")
			}

			p.Children = append(p.Children, n)
		}
	}

	return m[0], nil
}
