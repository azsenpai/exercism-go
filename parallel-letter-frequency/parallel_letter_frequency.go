package letter

import "sync"

// FreqMap records the frequency of each rune in a given text.
type FreqMap map[rune]int

// Frequency counts the frequency of each rune in a given text and returns this
// data as a FreqMap.
func Frequency(s string) FreqMap {
	m := FreqMap{}
	for _, r := range s {
		m[r]++
	}
	return m
}

func ConcurrentFrequency(l []string) FreqMap {
	var wg sync.WaitGroup
	data := make(chan FreqMap, len(l))

	wg.Add(len(l))

	for _, s := range l {
		go func(s string) {
			defer wg.Done()
			data <- Frequency(s)
		}(s)
	}

	wg.Wait()
	close(data)

	m := FreqMap{}

	for d := range data {
		for k, v := range d {
			m[k] += v
		}
	}

	return m
}
