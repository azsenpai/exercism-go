package foodchain

import (
	"fmt"
	"strings"
)

const (
	fmtStartPhrase = "I know an old lady who swallowed a %s."
	fmtPhrase      = "She swallowed the %s to catch the %s."
	fmtEndPhrase   = "I don't know why she swallowed the %s. Perhaps she'll die."
)

var data = []struct {
	Name, Action string
}{
	{"fly", ""},
	{"spider", "It wriggled and jiggled and tickled inside her."},
	{"bird", "How absurd to swallow a bird!"},
	{"cat", "Imagine that, to swallow a cat!"},
	{"dog", "What a hog, to swallow a dog!"},
	{"goat", "Just opened her throat and swallowed a goat!"},
	{"cow", "I don't know how she swallowed a cow!"},
	{"horse", "She's dead, of course!"},
}

// Verse return string
func Verse(n int) string {
	var b strings.Builder
	n--

	if n+1 == len(data) {
		fmt.Fprintf(&b, fmtStartPhrase+"\n%s", data[n].Name, data[n].Action)
	} else {
		fmt.Fprintf(&b, fmtStartPhrase, data[n].Name)

		if n > 0 {
			fmt.Fprintf(&b, "\n%s", data[n].Action)
		}

		for ; n > 0; n-- {
			name := data[n-1].Name
			if n-1 == 1 {
				name += " that wriggled and jiggled and tickled inside her"
			}
			fmt.Fprintf(&b, "\n"+fmtPhrase, data[n].Name, name)
		}

		fmt.Fprintf(&b, "\n"+fmtEndPhrase, data[0].Name)
	}

	return b.String()
}

// Verses return string
func Verses(i, j int) string {
	r := make([]string, j-i+1)
	for n := i; n <= j; n++ {
		r[n-i] = Verse(n)
	}
	return strings.Join(r, "\n\n")
}

// Song return string
func Song() string {
	return Verses(1, len(data))
}
