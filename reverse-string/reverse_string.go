package reverse

func Reverse(s string) string {
	r := []rune(s)

	for i, n, m := 0, len(r), len(r)/2; i < m; i++ {
		r[i], r[n-1-i] = r[n-1-i], r[i]
	}

	return string(r)
}
