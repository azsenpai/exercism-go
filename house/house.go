package house

import (
	"fmt"
	"strings"
)

var verseItems = map[int]struct {
	first, second string
}{
	1:  {"house that Jack built", ""},
	2:  {"malt", "lay in"},
	3:  {"rat", "ate"},
	4:  {"cat", "killed"},
	5:  {"dog", "worried"},
	6:  {"cow with the crumpled horn", "tossed"},
	7:  {"maiden all forlorn", "milked"},
	8:  {"man all tattered and torn", "kissed"},
	9:  {"priest all shaven and shorn", "married"},
	10: {"rooster that crowed in the morn", "woke"},
	11: {"farmer sowing his corn", "kept"},
	12: {"horse and the hound and the horn", "belonged to"},
}

func Actions(n int) (res string) {
	if n <= 1 {
		return
	}

	res = fmt.Sprintf("that %s the %s", verseItems[n].second, verseItems[n-1].first)

	if n == 2 {
		return
	}

	return res + "\n" + Actions(n-1)
}

func Verse(n int) string {
	res := fmt.Sprintf("This is the %s", verseItems[n].first)

	if n > 1 {
		res += "\n" + Actions(n)
	}

	res += "."

	return res
}

func Song() string {
	songs := make([]string, len(verseItems))

	for i := 0; i < len(songs); i++ {
		songs[i] = Verse(i + 1)
	}

	return strings.Join(songs, "\n\n")
}
