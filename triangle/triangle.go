package triangle

import "math"

type Kind uint8

const (
    NaT Kind = 0 // not a triangle
    Equ Kind = 1 // equilateral
    Iso Kind = 2 // isosceles
    Sca Kind = 3 // scalene
)

func KindFromSides(a, b, c float64) (k Kind) {
	for _, v := range []float64{a, b, c} {
		if math.IsNaN(v) || math.IsInf(v, 0) {
			return NaT
		}
	}

	if a > b {
		a, b = b, a
	}
	if b > c {
		b, c = c, b
	}
	if a > b {
		a, b = b, a
	}

	switch {
	case a <= 0 || a+b < c:
		k = NaT
	case a == b && b == c:
		k = Equ
	case a == b || a == c || b == c:
		k = Iso
	default:
		k = Sca
	}

	return
}
