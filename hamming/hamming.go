package hamming

import "fmt"

func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, fmt.Errorf("(%q, %q) length must be equal", a, b)
	}

	d := 0

	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			d += 1
		}
	}

	return d, nil
}
